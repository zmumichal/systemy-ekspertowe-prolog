function doCompile(input) {
    if (input == "") {
        alert("Please enter Prolog code or click one of the Samples");
        return;
    }

    var outputWriter = new YP.StringWriter();
    YP.tell(outputWriter);
    var Goal = new Variable();
    var VariableList = new Variable();
    for each (var l1 in parseGoal(input, Goal, VariableList)) {
        try {
            var gotMatch = false;
            for each (var l2 in YP.getIterator(Goal, null)) {
                gotMatch = true;
                if (YP.getValue(VariableList) != Atom.NIL) {
                    // We are showing values, so separate each match with ";".
                    writeValues(outputWriter, VariableList);
                    outputWriter.writeLine(";");
                }
            }

            if (gotMatch) {
                if (YP.getValue(VariableList) == Atom.NIL)
                // We didn't show any values.  So just write true after matching (one or more times).
                    outputWriter.writeLine("true");
            } else {
                outputWriter.writeLine("fail");
            }
        }
        catch (exception) {
            if (exception instanceof PrologException)
                outputWriter.writeLine(exception._term.toString());
            else
                throw exception;
        }
    }

    YP.told();
    console.log(outputWriter.toString());
    return outputWriter.resultNames
}

// Parse goalString and yield for each Goal, setting VariableList to a list of
// (variableAtom = Var).
function parseGoal(goalString, Goal, VariableList) {
    // The parser requires a newline or space at the end.
    // YP.see(new YP.StringReader(":- import('', [parent/2]). \n"+goalString + " "));
    YP.see(new YP.StringReader(goalString + " "));
    var TermList = new Variable();
    // parseInput set TermList to a list of f(Goal, VariableList).
    for each (var l1 in parseInput(TermList)) {
        // Close the input now before yielding.
        YP.seen();
        // Iterate through each member of TermList.
        for (TermList = YP.getValue(TermList);
             TermList instanceof Functor2 && TermList._name == Atom.DOT;
             TermList = YP.getValue(TermList._arg2)) {
            // Unify the head of the list with f(Goal, VariableList).
            for each (var l2 in YP.unify
            (TermList._arg1, new Functor2(Atom.F, Goal, VariableList)))
                yield false;
        }
        return;
    }
    // Close the input in case parseInput failed.
    YP.seen();
}

// Write to outputWriter each entry in VariableList which is a list of (variableAtom = Var).
function writeValues(outputWriter, VariableList) {
    for (VariableList = YP.getValue(VariableList);
         VariableList instanceof Functor2 && VariableList._name == Atom.DOT;
         VariableList = YP.getValue(VariableList._arg2)) {
        var variableAndValue = YP.getValue(VariableList._arg1);
        if (variableAndValue instanceof Functor2) {
            outputWriter.resultNames = outputWriter.resultNames || [];
            outputWriter.resultNames.push(variableAndValue._arg2._value._name);
            outputWriter.writeLine("");
            outputWriter.write(YP.getValue(variableAndValue._arg1).toString());
            outputWriter.write(" = ");
            outputWriter.write(YP.getValue(variableAndValue._arg2).toString());
        }
    }
}
