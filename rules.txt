assert((majsterkowicz   :- user_agrees('pytanie1'))).
assert((mysliciel       :- user_agrees('pytanie2'))).
assert((artysta         :- user_agrees('pytanie3'))).
assert((negocjator      :- user_agrees('pytanie4'))).
assert((porzadnis       :- user_agrees('pytanie5'))).
assert((precyzyjny      :- user_agrees('pytanie6'))).
assert((abstrakcyjny    :- user_agrees('pytanie7'))).
assert((wspierajacy     :- user_agrees('pytanie8'))).
assert((cierpliwy       :- user_agrees('pytanie9'))).
assert((podporzadkowujacy :- user_agrees('pytanie10'))).
assert((mechanik        :- user_agrees('pytanie11'))).
assert((opiekunczy      :- user_agrees('pytanie12'))).
assert((opinia_artysty  :- user_agrees('pytanie13'))).
assert((umie_przekazac  :- user_agrees('pytanie14'))).
assert((dokladny        :- user_agrees('pytanie15'))).
assert((narzedziolub    :- user_agrees('pytanie16'))).
assert((teoretyk        :- user_agrees('pytanie17'))).
assert((opinia_pomagajacego :- user_agrees('pytanie18'))).
assert((namawiacz       :- user_agrees('pytanie19'))).
assert((techniczny      :- user_agrees('pytanie20'))).
assert((wyobraznia_artysty :- user_agrees('pytanie21'))).
assert((nauczyciel      :- user_agrees('pytanie22'))).
assert((zreczny         :- user_agrees('pytanie23'))).
assert((zorganizowany   :- user_agrees('pytanie24'))).
assert((rozumiejacy     :- user_agrees('pytanie25'))).
assert((chec_artysty    :- user_agrees('pytanie26'))).
assert((pewny_tworca    :- user_agrees('pytanie27'))).
assert((pewny_lider     :- user_agrees('pytanie28'))).
assert((goal_oriented   :- user_agrees('pytanie29'))).

assert((elektrykDodatkowe   :- user_agrees('pytanie30'))).
assert((kierowcaDodatkowe   :- user_agrees('pytanie31'))).
assert((pilotDodatkowe   :- user_agrees('pytanie32'))).
assert((lesnikDodatkowe   :- user_agrees('pytanie33'))).

assert((astronomDodatkowe   :- user_agrees('pytanie33'))).
assert((archeologDodatkowe   :- user_agrees('pytanie34'))).
assert((chemikDodatkowe   :- user_agrees('pytanie35'))).
assert((programistaDodatkowe   :- user_agrees('pytanie36'))).

assert((aktorDodatkowe   :- user_agrees('pytanie37'))).
assert((architektDodatkowe   :- user_agrees('pytanie38'))).
assert((malarzDodatkowe   :- user_agrees('pytanie39'))).
assert((fotografDodatkowe   :- user_agrees('pytanie40'))).

assert((lekarzDodatkowe   :- user_agrees('pytanie41'))).
assert((policjantDodatkowe   :- user_agrees('pytanie42'))).
assert((kelnerDodatkowe   :- user_agrees('pytanie43'))).
assert((ratownikDodatkowe   :- user_agrees('pytanie44'))).

assert((prawnikDodatkowe   :- user_agrees('pytanie45'))).
assert((adwokatDodatkowe   :- user_agrees('pytanie46'))).
assert((doradcaDodatkowe   :- user_agrees('pytanie47'))).
assert((maklerDodatkowe   :- user_agrees('pytanie48'))).

assert((kasjerDodatkowe   :- user_agrees('pytanie49'))).
assert((statystykDodatkowe   :- user_agrees('pytanie50'))).
assert((ksiegowyDodatkowe   :- user_agrees('pytanie51'))).
assert((inkasentDodatkowe   :- user_agrees('pytanie52'))).

assert((at_least_positives(_, Expected, _) :- Expected =:= 0,!)).
assert((at_least_positives([Head|Tail], Expected, Total) :- Expected =< Total, Head, at_least_positives(Tail, Expected-1, Total-1),!)).
assert((at_least_positives([Head|Tail], Expected, Total) :- Expected =< Total, \+ Head, at_least_positives(Tail, Expected, Total-1))).

assert((typ_realistyczny :-
        at_least_positives([majsterkowicz, mechanik, narzedziolub, techniczny, zreczny],3,5)
)).

assert((typ_spoleczny :-
        at_least_positives([wspierajacy, cierpliwy, opiekunczy, opinia_pomagajacego, nauczyciel],3,5)
)).

assert((typ_badawczy :-
        at_least_positives([mysliciel, abstrakcyjny, teoretyk, rozumiejacy],2,4)
)).

assert((typ_przedsiębiorczy :-
        at_least_positives([negocjator, podporzadkowujacy, umie_przekazac, namawiacz, pewny_lider],3,5)
)).

assert((typ_artystyczny :-
        at_least_positives([artysta, opinia_artysty, wyobraznia_artysty, chec_artysty, pewny_tworca],3,5)
)).

assert((typ_konwencjonalny :-
        at_least_positives([porzadnis, precyzyjny, dokladny, zorganizowany, goal_oriented],3,5)
)).

assert((elektrykZawod :- typ_realistyczny, elektrykDodatkowe)).
assert((kierowcaZawod :- typ_realistyczny, kierowcaDodatkowe)).
assert((pilotZawod :- typ_realistyczny, pilotDodatkowe)).
assert((lesnikZawod :- typ_realistyczny, lesnikDodatkowe)).

assert((astronomZawod :- typ_badawczy, astronomDodatkowe)).
assert((archeologZawod :- typ_badawczy, archeologDodatkowe)).
assert((chemikZawod :- typ_badawczy, chemikDodatkowe)).
assert((programistaZawod :- typ_badawczy, programistaDodatkowe)).

assert((aktorZawod:- typ_artystyczny, aktorDodatkowe)).
assert((architektZawod :- typ_artystyczny, architektDodatkowe)).
assert((malarzZawod :- typ_artystyczny, malarzDodatkowe)).
assert((fotografZawod :- typ_artystyczny, fotografDodatkowe)).

assert((lekarzZawod :- typ_spoleczny, lekarzDodatkowe)).
assert((policjantZawod :- typ_spoleczny, policjantDodatkowe)).
assert((kelnerZawod :- typ_spoleczny, kelnerDodatkowe)).
assert((ratownikZawod :- typ_spoleczny, ratownikDodatkowe)).

assert((prawnikZawod :- typ_przedsiębiorczy, prawnikDodatkowe)).
assert((adwokatZawod :- typ_przedsiębiorczy, adwokatDodatkowe)).
assert((doradcaZawod :- typ_przedsiębiorczy, doradcaDodatkowe)).
assert((maklerZawod :- typ_przedsiębiorczy, maklerDodatkowe)).

assert((kasjerZawod :- typ_konwencjonalny, kasjerDodatkowe)).
assert((statystykZawod :- typ_konwencjonalny, statystykDodatkowe)).
assert((ksiegowyZawod :- typ_konwencjonalny, ksiegowyDodatkowe)).
assert((inkasentZawod :- typ_konwencjonalny, inkasentDodatkowe)).

assert((hipotetyzuj(elektryk) :- elektrykZawod)).
assert((hipotetyzuj(kierowca) :- kierowcaZawod)).
assert((hipotetyzuj(pilot) :- pilotZawod)).
assert((hipotetyzuj(lesnik) :- lesnikZawod)).
assert((hipotetyzuj(astronom) :- astronomZawod)).
assert((hipotetyzuj(archeolog) :- archeologZawod)).
assert((hipotetyzuj(chemik) :- chemikZawod)).
assert((hipotetyzuj(programista) :- programistaZawod)).
assert((hipotetyzuj(aktor) :- aktorZawod)).
assert((hipotetyzuj(architekt) :- architektZawod)).
assert((hipotetyzuj(malarz) :- malarzZawod)).
assert((hipotetyzuj(fotograf) :- fotografZawod)).
assert((hipotetyzuj(lekarz) :- lekarzZawod)).
assert((hipotetyzuj(policjant) :- policjantZawod)).
assert((hipotetyzuj(kelner) :- kelnerZawod)).
assert((hipotetyzuj(ratownik) :- ratownikZawod)).
assert((hipotetyzuj(prawnik) :- prawnikZawod)).
assert((hipotetyzuj(adwokat) :- adwokatZawod)).
assert((hipotetyzuj(doradca) :- doradcaZawod)).
assert((hipotetyzuj(makler) :- maklerZawod)).
assert((hipotetyzuj(kasjer) :- kasjerZawod)).
assert((hipotetyzuj(statystyk) :- statystykZawod)).
assert((hipotetyzuj(ksiegowy) :- ksiegowyZawod)).
assert((hipotetyzuj(inkasent) :- inkasentZawod)).